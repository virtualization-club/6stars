// Copyright (C) 2023 Sylvia BSD
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// This is a config file used for if you want to create a fork/distribution
// of 6Stars. Each value should be documented and easy to change

#define CONFIG_NAME "6Stars" //Name of the system distribution
#define CONFIG_REPO "https://gitlab.com/virtualization-club/6stars" //Where the repo for the distribution is hosted
