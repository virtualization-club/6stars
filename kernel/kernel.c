/*
    6Stars Operating System
    Copyright (C) 2023  Sylvia BSD

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdio.h>

#include <stdint.h>
#include <stddef.h>
#include <limine/limine.h>
#include "gfx/gfx.h"

#include "x86/limine_requests.h"

#include "x86/tables/gdt/gdt.h"
#include "x86/tables/idt/idt.h"

#include "x86/panic.h"

#include "vm/vm.h"

#define LIMINE_FRAMEBUFFER_RGB 1

static void done(void) {
    for (;;) {
        __asm__("hlt");
    }
}

void _start(void) {
    X86GDT_Init(); printf("Initialized global descriptor table\n");
    X86IDT_Init(); printf("Initialized interrupt descriptor table\n");

    
    //Anybody remove this notice and I *will* get your ass
    printf("\n\n    6Stars  Copyright (C) 2023  Sylvia BSD\n");
    printf("This program comes with ABSOLUTELY NO WARRANTY; for details type `show w\'. \n");
    printf("This is free software, and you are welcome to redistribute it\n");
    printf("under certain conditions\n");

    size_t framebuffers_count = framebuffer_request.response->framebuffer_count;
    printf("\nFound %i framebuffers.\n", framebuffers_count);
    for (size_t i = 0; i < framebuffers_count; i++) {
        struct limine_framebuffer *buffer = framebuffer_request.response->framebuffers[i];
        uint8_t framebuffer_mem_mode = buffer->memory_model;
        size_t framebuffer_mem_size  = (buffer->width * buffer->height) * framebuffer_mem_mode;
        printf("[FRAMEBUFFER#%i] %ix%i (Memsize=%i, Mode=%i, Pixels=%i).\n", i, buffer->width, buffer->height, framebuffer_mem_size, framebuffer_mem_mode, (int32_t) (framebuffer_mem_size / framebuffer_mem_mode));
    }

    framebuffer_t* buffer = Gfx_GetDefaultFramebuffer();

    for (int32_t i = 0; i < 100; i++)
        Gfx_PutColoredPixel(buffer, 500, 200 + i, (Gfx_Color32_t) { 0x00, 0x00, 0xFF, 0xFF });

    __asm__ __volatile__ ("int $0x0");

    done(); //THe temptation to let the CPU wandern is strong but I must
}