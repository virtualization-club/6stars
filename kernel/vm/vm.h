// Copyright (C) 2023 Sylvia BSD
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __KERNEL_VM_VM_H__
#define __KERNEL_VM_VM_H__

/**
 * Virtual machine runs using a sort-of harvard architecture
 * because having 2 seperate memorys, 1 read only and 1 read+write
 * is easier than having 1 read+write memory array that the program
 * is also in. Also means that letting a program wander memory is going 
 * to be a bit harder (and will have to deal with segfaults if it does)
*/

#include <stdint.h>

#include "instructions.h"

typedef enum KernelVM_Flags { //Flags
    FL_P = 1 << 0,
    FL_Z = 1 << 1,
    FL_N = 1 << 2
} KernelVM_Flags;

typedef enum KernelVM_Registers {
    //General purpose everyday registers to be used for important kernel
    //modules
    RG_0,  RG_1,  RG_2,  RG_3,  RG_4,  RG_5,  RG_6,  RG_7,  RG_8,  RG_9,
    RG_10, RG_11, RG_12, RG_13, RG_14, RG_15, RG_16, RG_17, RG_18, RG_19,
    RG_20, RG_21, RG_22, RG_23, RG_24, RG_25, RG_26, RG_27, RG_28, RG_29,
    RG_30, RG_31, RG_32,

    RG_RESULT, //Result register due to the VMs unique arch




    RG_PC, //Program counter
    RG_COND, //Condition flag
    RG_COUNT //Total count of registers for allocation
} KernelVM_Registers;

typedef struct KernelVM_VM {
    uint64_t *memory; //Dynamically allocated memory
    uint64_t *registers; //Registers
    uint64_t *programMem; //Program memory is read-onlyh


} KernelVM_VM;

#endif