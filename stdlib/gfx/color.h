/* 
    6Stars Operating System
    Copyright (C) 2023  Sylvia BSD

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __STDLIB_COLOR_H__
#define __STDLIB_COLOR_H__

#include <stdint.h>

typedef struct color32 {
    uint8_t r, g, b, a;
} __attribute__((packed)) Gfx_Color32_t;

typedef struct color24 {
    uint8_t r, g, b;
} __attribute__((packed)) Gfx_Color24_t;

Gfx_Color32_t Gfx_Color32FromI32(uint32_t rgba);
Gfx_Color24_t Gfx_Color24FromI24(uint32_t rgb);
uint32_t Gfx_I32FromColor32(Gfx_Color32_t color);
uint32_t Gfx_I32FromColor24(Gfx_Color24_t color);

#endif
