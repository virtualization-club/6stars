/* 
    6Stars Operating System
    Copyright (C) 2023  Sylvia BSD

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "gfx.h"

void Gfx_PutPixel(framebuffer_t* buffer, int32_t x, int32_t y) {
    if (buffer == NULL) return;
    ((uint32_t *) buffer->address)[buffer->width * y + x] = 0xFFFFFFFF;
}

void Gfx_PutColoredPixel(framebuffer_t* buffer, int32_t x, int32_t y, Gfx_Color32_t color) {
    if (buffer == NULL) return;
    ((uint32_t *) buffer->address)[buffer->width * y + x] = Gfx_I32FromColor32(color);
}

framebuffer_t* Gfx_GetDefaultFramebuffer() {
    size_t total_framebuffers = framebuffer_request.response->framebuffer_count;
    
    if (total_framebuffers <= 0) return NULL;

    return framebuffer_request.response->framebuffers[0];
}