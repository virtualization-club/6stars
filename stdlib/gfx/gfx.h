/* 
    6Stars Operating System
    Copyright (C) 2023  Sylvia BSD

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __STDLIB_GFX_H__
#define __STDLIB_GFX_H__

#include <inttypes.h>
#include "limine/limine.h"
#include "x86/limine_requests.h"
#include "color.h"

typedef struct limine_framebuffer framebuffer_t;

void Gfx_PutPixel(framebuffer_t* buffer, int32_t x, int32_t y);
void Gfx_PutColoredPixel(framebuffer_t* buffer, int32_t x, int32_t y, Gfx_Color32_t color);
framebuffer_t* Gfx_GetDefaultFramebuffer(); 


#endif