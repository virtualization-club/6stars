/* 
    6Stars Operating System
    Copyright (C) 2023  Sylvia BSD

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "stdio.h"


int putchar(char c) {
    //terminal_request.response->write(terminal, (const char*)c, 1);
    terminal_request.response->write(terminal, &c, 1);
    return c;
}

const char *itoh_map = "0123456789ABCDEF";

char* itoh(int i, char *buf) {
    int		n;
    int		b;
    int		z;
    int		s;
    
    if (sizeof(void*) == 4)
        s = 8;
    if (sizeof(void*) == 8)
        s = 16;
    
    for (z = 0, n = (s - 1); n > -1; --n) {
        b = (i >> (n * 4)) & 0xf;
        buf[z] = itoh_map[b];
        ++z;
    }
    
    buf[z] = 0;
    return buf;
}


void printf(const char* restrict fmt, ...) {
    const char 	*p;
    va_list 	argp;
    int 		i;
    char 		*s;
    char 		fmtbuf[256];

    va_start(argp, fmt);

    for(p = fmt; *p != '\0'; p++) {
        //putchar('w');
        if(*p != '%') {
            putchar(*p);
            continue;
        }

        switch(*++p) {
            case 'c':
                i = va_arg(argp, int);
                putchar(i);
                break;
            case 's':
                s = va_arg(argp, char *);
                //terminal_request.response->write(terminal, s, strlen(s));
                terminal_request.response->write(terminal, s, strlen(s));
                
                break;
            case 'x':
                i = va_arg(argp, int);
                s = itoh(i, fmtbuf);
                terminal_request.response->write(terminal, s, strlen(s));
                break;
            case 'd':
                char intStr[33]; // buffer *should* be more than enough
                i = va_arg(argp, int);
                int len = itoa(i, intStr, 10);
                intStr[len+1] = '\0';

                terminal_request.response->write(terminal, intStr, strlen(intStr));
                
                break;
            case '%':
                putchar('%');
                break;
        }
    }
    va_end(argp);
}