/* 
    6Stars Operating System
    Copyright (C) 2023  Sylvia BSD

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __STDLIB_STDIO_H__
#define __STDLIB_STDIO_H__

#include "stdlib.h"

#include <string.h>
#include <stdint.h>
#include <stddef.h>
#include <stdarg.h>
#include <limits.h>
#include <stdbool.h>

#include "limine/limine.h"

#include "x86/heap.h"
#include "x86/limine_requests.h"


#define EOF (-1)

int putchar(char c);
void printf(const char* restrict fmt, ...);

#endif