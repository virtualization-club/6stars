/* 
    6Stars Operating System
    Copyright (C) 2023  Sylvia BSD

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "string.h"

void* memcpy(void* restrict dstptr, const void* restrict srcptr, size_t size) {
    unsigned char* dst = (unsigned char*) dstptr;
    const unsigned char* src = (const unsigned char*) srcptr;
    for (size_t i = 0; i < size; i++)
        dst[i] = src[i];
    return dstptr;
}

void* memset(void* bufptr, int value, size_t size) {
    unsigned char* buf = (unsigned char*) bufptr;
    for (size_t i=0; i < size; i++)
        buf[i] = (unsigned char) value;

    return bufptr;
}

char * strcpy(char *strDest, const char *strSrc) {
    //assert(strDest!=NULL && strSrc!=NULL);
    if (strDest == NULL || strSrc == NULL) return NULL;
    
    char *temp = strDest;
    while((*strDest++=*strSrc++) != '\0');

    return temp;
}

size_t strlen(const char* str) {
    size_t len = 0;
    while (str[len]) len++;

    return len;
}