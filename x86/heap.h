#ifndef __X86_HEAP_H__
#define __X86_HEAP_H__

#include <stdint.h>
#include <stdbool.h>


typedef struct _KHEAPBLOCKBM {
    struct _KHEAPBLOCKBM                    *next;
    uint32_t                    size;
    uint32_t                    used;
    uint32_t                    bsize;
        uint32_t                                  lfb;
} KHEAPBLOCKBM;

typedef struct _KHEAPBM {
    KHEAPBLOCKBM            *fblock;
} KHEAPBM;

extern KHEAPBM heap;

void X86Heap_Init();
int X86Heap_AddBlock(KHEAPBM *heap, uintptr_t addr, uint32_t size, uint32_t bsize);
uint8_t X86Heap_GetNID(uint8_t a, uint8_t b);
void *X86Heap_Alloc(KHEAPBM *heap, uint32_t size);
void X86Heap_Free(KHEAPBM *heap, void *ptr);

#endif