/**
 * Copyright (C) 2023 sylviabsd
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "limine_requests.h"

volatile struct limine_framebuffer_request framebuffer_request = { .id = LIMINE_FRAMEBUFFER_REQUEST, .revision = 0 };
volatile struct limine_terminal_request terminal_request = { .id = LIMINE_TERMINAL_REQUEST, .revision = 0 };


struct limine_framebuffer *framebuffer;
struct limine_terminal *terminal;

static void done() {
    for (;;) __asm__ __volatile__ ("hlt");
}

void X86LimineRequests_Setup() {
    struct limine_framebuffer_response *framebuffer_response = framebuffer_request.response;

    if (framebuffer_response == NULL || framebuffer_response->framebuffer_count < 1) done();        //check for framebuffers
    if (terminal_request.response == NULL || terminal_request.response->terminal_count < 1) done(); //check for terminals

    framebuffer = framebuffer_response->framebuffers[0];
    terminal = terminal_request.response->terminals[0];

}