/* 
    6Stars Operating System
    Copyright (C) 2023  Sylvia BSD

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __X86_LIMINE_REQUESTS_H__
#define __X86_LIMINE_REQUESTS_H__

#include "limine.h"

#include <stddef.h>

extern struct limine_framebuffer *framebuffer;
extern struct limine_terminal *terminal;

extern volatile struct limine_framebuffer_request framebuffer_request;
extern volatile struct limine_terminal_request terminal_request;


void X86LimineRequests_Setup();

#endif