/**
 * Copyright (C) 2023 Sylvia BSD
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "panic.h"

void X86Panic_Panic(char* msg, int line, const char* function, const char* file) {
    //Need to specif ythe full thing otherwise the cpu gets flustured and throws an interrupt
    //because the compilers fucking stupid -S
    int width = terminal_request.response->terminals[0]->columns;
    int height = terminal_request.response->terminals[0]->rows;

    //Register info
    int cs, ds, fs, gs;
    uint64_t cr0, cr2, cr3, cr4;
    uint64_t rax, rbx, rcx, rdx, rbp, rsp, rsi, rdi;
    
    __asm__ __volatile__ ("\t movq %%cr0,%0" : "=r"(cr0)); __asm__ __volatile__ ("\t movq %%cr2,%0" : "=r"(cr2));
    __asm__ __volatile__ ("\t movq %%cr3,%0" : "=r"(cr3)); __asm__ __volatile__ ("\t movq %%cr4,%0" : "=r"(cr4));

    __asm__ __volatile__ ("\t movl %%cs,%0" : "=r"(cs)); __asm__ __volatile__ ("\t movl %%ds,%0" : "=r"(ds));
    __asm__ __volatile__ ("\t movl %%fs,%0" : "=r"(fs)); __asm__ __volatile__ ("\t movl %%gs,%0" : "=r"(gs));

    __asm__ __volatile__ ("\t movq %%rax,%0" : "=r"(rax)); __asm__ __volatile__ ("\t movq %%rbx,%0" : "=r"(rbx));
    __asm__ __volatile__ ("\t movq %%rcx,%0" : "=r"(rcx)); __asm__ __volatile__ ("\t movq %%rdx,%0" : "=r"(rdx));
    __asm__ __volatile__ ("\t movq %%rbp,%0" : "=r"(rbp)); __asm__ __volatile__ ("\t movq %%rsp,%0" : "=r"(rsp));
    __asm__ __volatile__ ("\t movq %%rsi,%0" : "=r"(rsi)); __asm__ __volatile__ ("\t movq %%rdi,%0" : "=r"(rdi));


    for (int i=0; i < width; i++) printf("-");
    printf("\n\tKernel panic exception! Something has gone critically wrong and system could not recover!");
    printf("\n\tError at %d:%s() in %s\n\n", line, function, file);


    printf("\n\tThe following error was provided by the system: \x1b[1;31m%s\x1b[0m", msg);
    printf("\n\tTry reporting the error at %s", CONFIG_REPO);
    printf("\n\tDumping system information now!\n");

    printf("\n\t[Registers]");
    printf("\n\tcs=%x/%d ds=%x/%d fs=%x/%d gs=%x/%d", cs, cs, ds, ds, fs, fs, gs, gs);
    printf("\n\tcr0=%x/%d cr2=%x/%d cr3=%x/%d cr4=%x/%d", cr0, cr0, cr2, cr2, cr3, cr3, cr4, cr4);
    printf("\n\trax=%x/%d rbx=%x/%d rcx=%x/%d rdx=%x/%d", rax, rax, rbx, rbx, rcx, rcx, rdx, rdx);
    printf("\n\trbp=%x/%d rsp=%x/%d rsi=%x/%d rdi=%x/%d", rbp, rbp, rsp, rsp, rsi, rsi, rdi, rdi);

    for (int i=0; i < height - 14; i++) printf("\n");
}