// Copyright (C) 2023 Sylvia BSD
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __X86_PANIC_H__
#define __X86_PANIC_H__

#include <stdio.h>
#include <stdlib.h>
#include <config.h>

#include "limine_requests.h"

/**
 * @brief Easier to use panic function where it does shit for you. msg is err msg
 * 
 */
#define panic(msg) X86Panic_Panic(msg, __LINE__, __FUNCTION__, __FILE__)

/**
 * @brief I *really* wont recommend using it but use it if you want to ig
 * 
 * @param msg Panic message
 * @param line Line the error occured. Usually __LINE__
 * @param function Function the error occured. Usually __FUNCTION__
 * @param file Source file where it called. Usually __FILE__
 */
void X86Panic_Panic(char* msg, int line, const char* function, const char* file);

#endif