/**
 * Copyright (C) 2023 Sylvia BSD
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "pic.h"


void X86PIC_Wait() {
    outb(0x80, 0);
}

void X86Pic_Init(uint8_t start_pic_master, uint8_t start_pic_slave) {
    outb(0x20, 0x01 | 0x10 | 0x04);
    X86PIC_Wait();
    outb(0xa0, 0x01 | 0x10 | 0x04);
    X86PIC_Wait();

    outb(0x21, start_pic_master);
    X86PIC_Wait();
    outb(0xa1, start_pic_slave);
    X86PIC_Wait();

    outb(0x21, 4);
    X86PIC_Wait();
    outb(0xa1, 2);
    X86PIC_Wait();

    outb(0x21, 0x01);
    X86PIC_Wait();
    outb(0xa1, 0x01);
    X86PIC_Wait();

    outb(0x21, 0xff);
    outb(0xa1, 0xff);
    __asm__ __volatile__ ("sti");
}
