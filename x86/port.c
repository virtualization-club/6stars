/**
 * Copyright (C) 2023 Sylvia BSD
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "port.h"

void outb(uint16_t port, uint8_t val){
    __asm__ __volatile__ ("outb %0, %1" :: "a"(val), "Nd"(port));
}

uint8_t inb(uint16_t port){
    uint8_t ret;
    __asm__ __volatile__ ("inb %1, %0" : "=a"(ret) : "Nd"(port));
    return ret;
}


void outw(uint16_t port, uint16_t val){
    __asm__ __volatile__ ("outw %0, %1" :: "a"(val), "Nd"(port));
}

uint16_t inw(uint16_t port){
    uint16_t ret;
    __asm__ __volatile__ ("inw %1, %0" : "=a"(ret) : "Nd"(port));
    return ret;
}


void outl(uint16_t port, uint32_t val){
    __asm__ __volatile__ ("outl %0, %1" :: "a"(val), "Nd"(port));
}

uint32_t inl(uint16_t port){
    uint32_t ret;
    __asm__ __volatile__ ("inl %1, %0" : "=a"(ret) : "Nd"(port));
    return ret;
}