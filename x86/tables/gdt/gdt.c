/**
 * Copyright (C) 2023 Sylvia BSD
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gdt.h"

X86GDT_Gdt_t  X86GDT_Gdt;
X86GDT_Gdtr_t X86GDT_Gdtr;

void X86GDT_Reload() {
    __asm__ __volatile__ (
        "lgdt %0\n\t"
        "push $0x28\n\t"
        "lea 1f(%%rip), %%rax\n\t"
        "push %%rax\n\t"
        "lretq\n\t"
        "1:\n\t"
        "mov $0x30, %%eax\n\t"
        "mov %%eax, %%ds\n\t"
        "mov %%eax, %%es\n\t"
        "mov %%eax, %%fs\n\t"
        "mov %%eax, %%gs\n\t"
        "mov %%eax, %%ss\n\t"
        :
        : "m"(X86GDT_Gdtr)
        : "rax", "memory"
    );
}

void X86GDT_Init() {
    
    X86GDT_Gdt.descriptors[0].limit       = 0;
    X86GDT_Gdt.descriptors[0].base_low16  = 0;
    X86GDT_Gdt.descriptors[0].base_mid8   = 0;
    X86GDT_Gdt.descriptors[0].access      = 0;
    X86GDT_Gdt.descriptors[0].granularity = 0;
    X86GDT_Gdt.descriptors[0].base_high8  = 0;

    // Kernel code 16.
    X86GDT_Gdt.descriptors[1].limit       = 0xffff;
    X86GDT_Gdt.descriptors[1].base_low16  = 0;
    X86GDT_Gdt.descriptors[1].base_mid8   = 0;
    X86GDT_Gdt.descriptors[1].access      = 0b10011010;
    X86GDT_Gdt.descriptors[1].granularity = 0b00000000;
    X86GDT_Gdt.descriptors[1].base_high8  = 0;

    // Kernel data 16.
    X86GDT_Gdt.descriptors[2].limit       = 0xffff;
    X86GDT_Gdt.descriptors[2].base_low16  = 0;
    X86GDT_Gdt.descriptors[2].base_mid8   = 0;
    X86GDT_Gdt.descriptors[2].access      = 0b10010010;
    X86GDT_Gdt.descriptors[2].granularity = 0b00000000;
    X86GDT_Gdt.descriptors[2].base_high8  = 0;

    // Kernel code 32.
    X86GDT_Gdt.descriptors[3].limit       = 0xffff;
    X86GDT_Gdt.descriptors[3].base_low16  = 0;
    X86GDT_Gdt.descriptors[3].base_mid8   = 0;
    X86GDT_Gdt.descriptors[3].access      = 0b10011010;
    X86GDT_Gdt.descriptors[3].granularity = 0b11001111;
    X86GDT_Gdt.descriptors[3].base_high8  = 0;

    // Kernel data 32.
    X86GDT_Gdt.descriptors[4].limit       = 0xffff;
    X86GDT_Gdt.descriptors[4].base_low16  = 0;
    X86GDT_Gdt.descriptors[4].base_mid8   = 0;
    X86GDT_Gdt.descriptors[4].access      = 0b10010010;
    X86GDT_Gdt.descriptors[4].granularity = 0b11001111;
    X86GDT_Gdt.descriptors[4].base_high8  = 0;

    // Kernel code 64.
    X86GDT_Gdt.descriptors[5].limit       = 0;
    X86GDT_Gdt.descriptors[5].base_low16  = 0;
    X86GDT_Gdt.descriptors[5].base_mid8   = 0;
    X86GDT_Gdt.descriptors[5].access      = 0b10011010;
    X86GDT_Gdt.descriptors[5].granularity = 0b00100000;
    X86GDT_Gdt.descriptors[5].base_high8  = 0;

    // Kernel data 64.
    X86GDT_Gdt.descriptors[6].limit       = 0;
    X86GDT_Gdt.descriptors[6].base_low16  = 0;
    X86GDT_Gdt.descriptors[6].base_mid8   = 0;
    X86GDT_Gdt.descriptors[6].access      = 0b10010010;
    X86GDT_Gdt.descriptors[6].granularity = 0;
    X86GDT_Gdt.descriptors[6].base_high8  = 0;

    // SYSENTER related dummy entries
    X86GDT_Gdt.descriptors[7] = (X86GDT_GdtDescriptor_t){0};
    X86GDT_Gdt.descriptors[8] = (X86GDT_GdtDescriptor_t){0};

    // User code 64.
    X86GDT_Gdt.descriptors[9].limit       = 0;
    X86GDT_Gdt.descriptors[9].base_low16  = 0;
    X86GDT_Gdt.descriptors[9].base_mid8   = 0;
    X86GDT_Gdt.descriptors[9].access      = 0b11111010;
    X86GDT_Gdt.descriptors[9].granularity = 0b00100000;
    X86GDT_Gdt.descriptors[9].base_high8  = 0;

    // User data 64.
    X86GDT_Gdt.descriptors[10].limit       = 0;
    X86GDT_Gdt.descriptors[10].base_low16  = 0;
    X86GDT_Gdt.descriptors[10].base_mid8   = 0;
    X86GDT_Gdt.descriptors[10].access      = 0b11110010;
    X86GDT_Gdt.descriptors[10].granularity = 0;
    X86GDT_Gdt.descriptors[10].base_high8  = 0;

    // TSS.
    X86GDT_Gdt.tss.length       = 104;
    X86GDT_Gdt.tss.base_low16   = 0;
    X86GDT_Gdt.tss.base_mid8    = 0;
    X86GDT_Gdt.tss.flags1       = 0b10001001;
    X86GDT_Gdt.tss.flags2       = 0;
    X86GDT_Gdt.tss.base_high8   = 0;
    X86GDT_Gdt.tss.base_upper32 = 0;
    X86GDT_Gdt.tss.reserved     = 0;

    // Set the pointer.
    X86GDT_Gdtr.limit = sizeof(X86GDT_Gdt) - 1;
    X86GDT_Gdtr.base  = (uint64_t)&X86GDT_Gdt;
}