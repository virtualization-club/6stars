// Copyright (C) 2023 Sylvia BSD
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __X86_TABLES_GDT_GDT_H__
#define __X86_TABLES_GDT_GDT_H__

#include <stdint.h>



typedef struct { //Data contained in a single 64-bit GDT descriptor
    uint16_t limit;
    uint16_t base_low16;
    uint8_t  base_mid8;
    uint8_t  access;
    uint8_t  granularity;
    uint8_t  base_high8;
} X86GDT_GdtDescriptor_t;

typedef struct {
    uint16_t length;
    uint16_t base_low16;
    uint8_t  base_mid8;
    uint8_t  flags1;
    uint8_t  flags2;
    uint8_t  base_high8;
    uint32_t base_upper32;
    uint32_t reserved;
} X86GDT_TssDescriptor_t;

typedef struct { //GDT
    X86GDT_GdtDescriptor_t descriptors[11];
    X86GDT_TssDescriptor_t tss;
} X86GDT_Gdt_t;

typedef struct { //GDT pointer
    uint16_t limit;
    uint64_t base;
} X86GDT_Gdtr_t;

void X86GDT_Reload();

void X86GDT_Init();


extern X86GDT_Gdt_t  X86GDT_Gdt;
extern X86GDT_Gdtr_t X86GDT_Gdtr;

#endif