// Copyright (C) 2023 Sylvia BSD
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __X86_TABLES_IDT_HANDLERS_DOUBLEFAULT_H__
#define __X86_TABLES_IDT_HANDLERS_DOUBLEFAULT_H__

#include "x86/panic.h"

//Ok look what fucking else can I do huh?
void X86IdtHandlersDoubleFault_Exception();

#endif