/**
 * Copyright (C) 2023 Sylvia BSD
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "idt.h"

#include "handlers/divisionBy0.h"
#include "handlers/doubleFault.h"

X86IDT_IdtEntry X86IDT_Idt[256];
void* X86IDT_Isr[256];
uint8_t X86IDT_PanicVectorIpiVector;

static void X86IDT_ExceptionHandler() {
    printf("Hello exceptions!");
}

void X86IDT_RegisterHandler(uint8_t vector, void* handler, uint8_t flags) {
    uint64_t handlerInt = (uint64_t)handler;

    X86IDT_Idt[vector] = (X86IDT_IdtEntry){
        .offset_low = (uint16_t)handlerInt,
        .selector = 0x28,
        .ist = 0,
        .flags = flags,
        .offset_mid = (uint16_t)(handlerInt >> 16),
        .offset_hi = (uint32_t)(handlerInt >> 32),
        .reserved = 0
    };
}

uint8_t X86IDT_AllocateVector() { return 33; }

void X86IDT_Reload(void) {
    X86IDT_Idtr idtr = {
        .limit = sizeof(X86IDT_Idt) - 1,
        .base = (uint64_t)X86IDT_Idt
    };

    __asm__ __volatile__ ("lidt %0" :: "m"(idtr) : "memory");
}


void X86IDT_Init() {
    X86Pic_Init(0x20, 0x70);


    for (uint16_t i=0; i < 256; i++)
        switch (i) {
            case 0x8: X86IDT_RegisterHandler(i, (void*)X86IdtHandlersDoubleFault_Exception, 0x8e); break; //Double fault
            default:
                X86IDT_RegisterHandler(i, (void*)X86IDT_ExceptionHandler ,0x8e);
        }

    X86IDT_Reload();
}
