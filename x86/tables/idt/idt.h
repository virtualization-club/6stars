// Copyright (C) 2023 Sylvia BSD
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __X86TABLES_IDT_IDT_H__
#define __X86TABLES_IDT_IDT_H__

#include <stdint.h>
#include <stdio.h>

#include <x86/pic.h>

typedef struct {
    uint16_t offset_low;
    uint16_t selector;
    uint8_t ist;
    uint8_t flags;
    uint16_t offset_mid;
    uint32_t offset_hi;
    uint32_t reserved;
} X86IDT_IdtEntry;

typedef struct __attribute__((packed)) {
    uint16_t limit;
    uint64_t base;
} X86IDT_Idtr; //IDT pointer

extern X86IDT_IdtEntry X86IDT_Idt[256];
extern void* X86IDT_Isr[256];
extern uint8_t X86IDT_PanicVectorIpiVector;

void X86IDT_RegisterHandler(uint8_t vector, void* handler, uint8_t flags);
uint8_t X86IDT_AllocateVector();
void X86IDT_Reload(void);

void X86IDT_Init(void);

#endif